cc = nasm
once: main.asm
	${cc} -f elf main.asm
	ld -m elf_i386 main.o -o main
dump: main
	objdump -S -M intel -d main > obj.dump
	cat obj.dump
hex: main
	hexeditor main
run: main
	./main