atoi:
    push ebx,
    push ecx
    push edx
    push esi
    mov esi, eax
    xor eax,eax
    xor ecx, ecx
    .multiplyLoop:
        xor ebx, ebx
        mov bl, [esi+ecx]
        cmp bl,48
        jl .finished
        cmp bl,57
        jg .finished
        sub bl,48
        add eax, ebx
        mov ebx,10
        mul ebx
        inc ecx
        jmp .multiplyLoop
    .finished:
        cmp ecx, 0
        je .restore
        mov ebx,10
        div ebx
    .restore:
        pop esi
        pop edx
        pop ecx
        pop edx
        ret


iprint:
    push eax
    push edx
    push ecx
    push esi
    xor ecx,ecx
    .devideLoop:
        inc ecx
        xor edx,edx
        mov esi,10
        idiv esi
        add edx,48
        push edx
        cmp eax,0
        jnz .devideLoop
    .printLoop:
        dec ecx
        mov eax, esp
        call sprint
        pop eax
        cmp ecx,0
        jnz .printLoop
    pop esi
    pop ecx
    pop edx
    pop eax
    ret
iprintLF:
    call iprint
    push eax
    mov eax, 0Ah
    push eax
    mov eax, esp
    call sprint
    pop eax
    pop eax
    ret
strlen:
    push ebx
    mov ebx, eax
    .nextchar:
        cmp byte [eax], 0
        je .finished
        inc eax
        jmp .nextchar
    .finished:
        sub eax, ebx
        pop ebx
        ret

sprint:
    push ecx
    push edx
    push ebx
    push eax

    call strlen

    mov edx,eax
    pop eax
    mov ecx, eax
    mov eax,4
    mov ebx,1
    int 0x80

    pop ebx
    pop edx
    pop ecx
    ret
sprintLF:
    call sprint
    push eax
    mov eax, 0Ah
    push eax
    mov eax, esp
    call sprint
    pop eax
    pop eax
    ret
quit:
    xor eax, eax
    inc eax
    xor ebx,ebx
    int 0x80
    ret